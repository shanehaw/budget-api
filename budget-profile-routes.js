const express = require('express'),
    jwt = require('express-jwt'),
    jwtDecoder = require('jsonwebtoken'),
    fs = require('fs');

const app = module.exports = express.Router();

let profileBudgets = loadProfileBudgetsFromFile();

function loadProfileBudgetsFromFile() {
    let fileProfileBudgets = [];
    const budgetFolders = fs.readdirSync("./data/budgets");
    for(let i = 0; i < budgetFolders.length; i++) {
        const budgetFolder = budgetFolders[i];
        const budgets = fs.readdirSync(`./data/budgets/${budgetFolder}`);
        for(let j = 0; j < budgets.length; j++) {
            const budget = budgets[j];
            const content = fs.readFileSync(`./data/budgets/${budgetFolder}/${budget}`);
            fileProfileBudgets.push(JSON.parse(content));
        }
    }
    return fileProfileBudgets;
}

const jwtCheck = jwt({
    secret: 'super-secret',
    audience: 'budget',
    issuer: 'budget-api'
});

function matchUserIds() {
    return function (req, res, next) {
        const profileId = req.params.profileId;
        const token = req.headers.authorization.split(" ")[1];
        const payload = jwtDecoder.decode(token);
        if (profileId != payload.id) {
            res.status(403).send();
        } else {
            next();
        }
    };
}

app.use('/api/profile/:profileId', jwtCheck, matchUserIds());



function findBudgetsForUser(userid) {
    const budgets = [];
    for(let i = 0; i < profileBudgets.length; i++) {
        const budget = profileBudgets[i];
        if(budget.userid == userid) {
            budgets.push(budget);
        }
    }
    return budgets;
}

app.get('/api/profile/:profileId/budgets', function (req, res) {
    const userid = req.params.profileId;
    const budgets = findBudgetsForUser(userid);
    const payload = JSON.stringify(budgets);
    setTimeout((function() {res.status(200).send(payload)}), 1000);
});

app.put('/api/profile/:profileId/budgets/:budgetKey', function(req, res) {
    const userid = req.params.profileId;
    const budgetKey = req.params.budgetKey;
    const budget = req.body;

    if(!userid || !budgetKey || !budget || userid <= 0 || budgetKey <= 0 || budget.length === 0) {
        res.status(400).send()
    } else {
        let foundBudget = false;
        for(let i = 0; i < profileBudgets.length; i++) {
            const curBudget = profileBudgets[i];
            if(curBudget.userid == userid && curBudget.key == budgetKey) {

                if(!fs.existsSync(`./data/budgets/${userid}/${budgetKey}.json`)) {
                    console.error(`Cannot find: ./data/budgets/${userid}/${budgetKey}.json`);
                    res.status(500).send();
                    return;
                }

                foundBudget = true;
                fs.truncateSync(`./data/budgets/${userid}/${budgetKey}.json`);
                fs.writeFileSync(`./data/budgets/${userid}/${budgetKey}.json`, JSON.stringify(budget, null, 2));
                break;
            }
        }

        if(!foundBudget) {
            console.error(`Budget for user: ${userid} and key: ${budgetKey} not found`);
            res.status(404).send();
            return;
        }

        profileBudgets = loadProfileBudgetsFromFile();
        setTimeout((function() {res.status(200).send()}), 500);
    }
});

app.delete('/api/profile/:profileId/budgets/:budgetKey', function(req, res) {
    const userid = req.params.profileId;
    const budgetKey = req.params.budgetKey;
    if(!userid || !budgetKey || userid <= 0 || budgetKey <= 0) {
        res.status(400).send()
    } else {
        let foundBudget = false;
        for(let i = 0; i < profileBudgets.length; i++) {
            const curBudget = profileBudgets[i];
            if(curBudget.userid == userid && curBudget.key == budgetKey) {

                if(!fs.existsSync(`./data/budgets/${userid}/${budgetKey}.json`)) {
                    console.error(`Cannot find: ./data/budgets/${userid}/${budgetKey}.json`);
                    res.status(500).send();
                    return;
                }

                foundBudget = true;
                fs.unlinkSync(`./data/budgets/${userid}/${budgetKey}.json`);
                break;
            }
        }

        if(!foundBudget) {
            console.error(`Budget for user: ${userid} and key: ${budgetKey} not found`);
            res.status(404).send();
            return;
        }

        profileBudgets = loadProfileBudgetsFromFile();
        setTimeout((function() {res.status(200).send()}), 500);
    }
});

app.post('/api/profile/:profileId/budgets', function(req, res) {
    const userid = req.params.profileId;
    const reqBody = req.body;
    if(!userid || !reqBody) {
        res.status(400).send()
    } else {
        const userBudgets = findBudgetsForUser(userid);
        let maxUserBudgetKey = 0;
        for(let i = 0; i < userBudgets.length; i++) {
            const userBudget = userBudgets[i];
            if(userBudget.key > maxUserBudgetKey) {
                maxUserBudgetKey = userBudget.key
            }
        }

        const newBudget = {
            userid,
            key: maxUserBudgetKey + 1,
            name: reqBody.name || `New Budget - ${maxUserBudgetKey + 1}`,
            date: reqBody.date || new Date(),
            income: [],
            expenses: {
                fixed: [],
                variable: []
            }
        };

        if(!fs.existsSync(`./data/budgets/${userid}`)) {
            fs.mkdirSync(`./data/budgets/${userid}`);
        }

        fs.writeFileSync(`./data/budgets/${userid}/${newBudget.key}.json`, JSON.stringify(newBudget, null, 2));
        profileBudgets = loadProfileBudgetsFromFile();

        setTimeout((function() {res.status(200).send(newBudget)}), 500);
    }
});
