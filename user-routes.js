var express = require('express'),
    _       = require('lodash'),
    jwt     = require('jsonwebtoken'),
    fs      = require('fs');

var app = module.exports = express.Router();

var users = loadUsersFromFile();

function loadUsersFromFile() {
  let fileUsers = [];
  const userFiles = fs.readdirSync("./data/users");
  for(let i = 0; i < userFiles.length; i++) {
    const user = userFiles[i];
    const content = fs.readFileSync(`./data/users/${user}`);
    fileUsers.push(JSON.parse(content));
  }
  return fileUsers;
}

function createIdToken(user) {
  return jwt.sign({
    id: user.id,
    username: user.username,
    iss: 'budget-api',
    aud: 'budget'
  }, 'super-secret', { expiresIn: 60*60*5 });
}

function getUserScheme(req) {
  
  var username;
  var type;
  var userSearch = {};

  // The POST contains a username and not an email
  if(req.body.username) {
    username = req.body.username;
    type = 'username';
    userSearch = { username: username };
  }
  // The POST contains an email and not an username
  else if(req.body.email) {
    username = req.body.email;
    type = 'email';
    userSearch = { email: username };
  }

  return {
    username: username,
    type: type,
    userSearch: userSearch
  }
}

app.post('/users', function(req, res) {
  
  var userScheme = getUserScheme(req);  

  if (!userScheme.username || !req.body.password) {
    return res.status(400).send("You must send the username and the password");
  }

  if (_.find(users, userScheme.userSearch)) {
   return res.status(400).send("A user with that username already exists");
  }

  var profile = _.pick(req.body, userScheme.type, 'password', 'extra');
  profile.id = (_.maxBy(users, 'id') || { id: 0}).id + 1;

  fs.writeFileSync(`./data/users/${profile.id}.json`, JSON.stringify(profile, null, 2));
  users = loadUsersFromFile();

  setTimeout((function() {
    res.status(201).send({
      id_token: createIdToken(profile),
      userid: profile.id
    });
  }), 1000);
});

app.post('/sessions/create', function(req, res) {

  var userScheme = getUserScheme(req);

  if (!userScheme.username || !req.body.password) {
    return res.status(400).send("You must send the username and the password");
  }

  var user = _.find(users, userScheme.userSearch);
  
  if (!user) {
    return res.status(401).send("The username or password don't match");
  }

  if (user.password !== req.body.password) {
    return res.status(401).send("The username or password don't match");
  }

  res.status(201).send({
    id_token: createIdToken(user),
    userid: user.id
  });

});
